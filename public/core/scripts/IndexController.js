
angular.module('olivierjeannerose', []).controller('IndexCtrl', ['$scope', function ($scope) {
	
	$scope.displayHome  = false;
	$scope.displayEnter = true;
	$scope.dayNight 	= false;
	$scope.dayNightText = "DAY";

	$scope.changeDayNight = function() {
		if($scope.dayNight)
		{
			$scope.dayNightText = "DAY";
			$scope.dayNight 	= false; 
		}
		else {
			$scope.dayNightText = "NIGHT";
			$scope.dayNight 	= true;
		}
	}
}]);

/*function IndexCtrl($scope) {

	$scope.test = "Triptrip";
};*/


