<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stilllife extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header.phtml');
		$this->load->view('stilllife.phtml');
	}
}