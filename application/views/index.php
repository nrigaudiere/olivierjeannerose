<div class="container-fluid" >
	<div class="row visible-xs visible-sm animated fadeIn" ng-show="displayHome">
		<div class="col-md-4">
			<img class="home-image" src="public/core/images/common/home1.png" alt="Still Life">
		</div>
		<div class="col-md-4">
			<img class="home-image" src="public/core/images/common/home2.png" alt="Beauty">
		</div>
		<div class="col-md-4">
			<img class="home-image" src="public/core/images/common/home3.png" alt="Portrait">
		</div>
	</div>
	<div class="row visible-md visible-lg animated fadeIn" ng-show="displayHome">
		<div class="col-md-10 col-md-offset-1">
			<img class="home-image-full" src="public/core/images/common/homefull.jpg" alt="Home Full">
		</div>
	</div>
	
</div>

